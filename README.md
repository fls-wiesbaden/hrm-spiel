# 2019-hochschule-rhein-main-java-spiel
A little game programmed with a game engine of the Hochschulen-Rhein-Main in Wiesbaden

When we visited the Rhein Main universities with the upper school as part of our computer science classes, we were asked to program a game ourselves using Java and a game engine provided to us. I made a small Space Invaders copy. It is not quite finished yet and I haven't really worked on it since then.
