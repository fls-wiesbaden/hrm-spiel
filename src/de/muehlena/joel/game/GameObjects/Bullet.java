package de.muehlena.joel.game.GameObjects;

import name.panitz.game.framework.*;

public class Bullet<I> extends AbstractGameObject<I>{

	public Bullet(double width, double height, Vertex position, Vertex velocity) {
		super(width, height, position, velocity);
	}

	@Override
	public void paintTo(GraphicsTool<I> g) {
		g.drawLine(getPos().x, getPos().y, getPos().x + getHeight(), getPos().y + getWidth());
		g.setColor(0, 1, 0);
		
	}

}
