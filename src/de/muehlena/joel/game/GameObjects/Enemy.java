package de.muehlena.joel.game.GameObjects;

import name.panitz.game.framework.*;

public class Enemy<I> extends AbstractGameObject<I>{


	private boolean collided = false;

	public Enemy(double width, double height, Vertex position, Vertex velocity) {
		super(width, height, position, velocity);
		// TODO Auto-generated constructor stub
	}
	
	public boolean getCollided() {
		return this.collided;
	}
	
	public void setcollided(boolean collided) {
		this.collided = collided;
	}

	@Override
	public void paintTo(GraphicsTool<I> g) {
		g.fillRect(getPos().x, getPos().y, getWidth(), getHeight());
		g.setColor(0,1,0);
	}

}
