package de.muehlena.joel.game;

import java.util.ArrayList;
import java.util.List;

import de.muehlena.joel.game.GameObjects.Alien;
import de.muehlena.joel.game.GameObjects.Bullet;
import de.muehlena.joel.game.GameObjects.Enemy;
import name.panitz.game.framework.*;

public class Spiel<I, S> extends AbstractGame<I, S>{

	private final static int HEIGHT = 600;
	private final static int WIDTH = 800;
	private final int BOTTOM = HEIGHT - 150;
	
	int hits = 0;
	int live = 5;
	
	private boolean isRunning = false;
	
	private List<GameObject<I>> hintergrund = new ArrayList<>();
	private List<Bullet<I>> bullets  = new ArrayList<>();
	private List<Enemy<I>> enemies = new ArrayList<>();
	private List <TextObject<I>> menuText= new ArrayList<>();
	private List <TextObject<I>> gametext= new ArrayList<>();
	
	
	
	public Spiel() {
		super(new Alien<>("alienS.png"), WIDTH, HEIGHT);
		getPlayer().setHeight(20);
		initPosition();
		
		hintergrund.add(new ImageObject<>("bg.jpg"));
		menuText.add(new TextObject<I>(new Vertex(WIDTH / 2 - 200, HEIGHT / 2), "Press a Key to start Game", "Arial", 40));
		
		gametext.add(new TextObject<I>(new Vertex(10, 25), "Hits Placed: " + hits, "Arial", 20));
		gametext.add(new TextObject<I>(new Vertex(10, 55), "Live(s): " + live, "Arial", 20));
		
		getGOss().add(hintergrund);
		getGOss().add(bullets);
		getGOss().add(enemies);
		getGOss().add(menuText);
		getGOss().add(gametext);
	}
	
	public void initPosition() {
		getPlayer().getPos().moveTo(new Vertex(WIDTH / 2 - getPlayer().getWidth() / 2, BOTTOM));
	}

	@Override
	public void doChecks() {
		
		if(isRunning && menuText.size() == 1) {
			menuText.remove(0);
		}
		
		if(isRunning) {
			//check if bullet hits enemy
			if(enemies.size() < 4) {
				enemies.add(new Enemy<>(40, 40, new Vertex(Math.floor(Math.random() * (WIDTH - 30)  + 1), 0), new Vertex(0, Math.floor(Math.random() * 2 + 1))));
			}

			for(int i=0;i<bullets.size();i++) {
				if(bullets.get(i).getPos().y < -10) {
					bullets.remove(i);
				}
			}

			for(int i = 0;i < enemies.size();i++) {
				
				if(enemies.get(i).touches(getPlayer()) && !enemies.get(i).getCollided()) {
					live--;
					enemies.get(i).setcollided(true);
					gametext.get(1).text = "Live(s): " + live;
				}
				
				if(enemies.get(i).getPos().y > HEIGHT + 20) {
					enemies.remove(i);
				}
			}

			for(int j=0;j<bullets.size();j++) {
				for(int i=0;i<enemies.size();i++) {
					if(bullets.get(j).touches(enemies.get(i))) {
						enemies.remove(i);
						bullets.remove(j);
						hits++;
						gametext.get(0).text = "Hits: " + hits;
						break;
					}
				}
			}

		}
		
		if(live <= 0) {
			isRunning = false;
			menuText.clear();
			enemies.clear();
			bullets.clear();
			menuText.add(new TextObject<I>(new Vertex(WIDTH / 2 - 200, HEIGHT / 2), "Game over!", "Arial", 40));
			menuText.add(new TextObject<I>(new Vertex(WIDTH / 2 - 200, HEIGHT / 2 + 50), "Press a Key to start Game", "Arial", 40));
		}
		
	}
	
	public void shootBullet() {
		bullets.add(new Bullet<>(20, 15, new Vertex(getPlayer().getPos().x + getPlayer().getWidth() / 2, getPlayer().getPos().y), new Vertex(0, -4)));
	}
	
	@Override
	public void keyPressedReaction(KeyCode code) {
		if(code != null) {
			
			if(!isRunning) {
				isRunning = true;
				live = 5;
				hits = 0;

				if(menuText.size() == 2) {
					menuText.clear();
				}
			}else {
				switch (code) {
					case RIGHT_ARROW:
						if(getPlayer().getPos().x + 5 <= WIDTH - getPlayer().getWidth() - 10) {
							getPlayer().getPos().moveTo(new Vertex(getPlayer().getPos().x + 10, BOTTOM));
						}
						break;
					case LEFT_ARROW:
						if(getPlayer().getPos().x - 5 >= 10) {
							getPlayer().getPos().moveTo(new Vertex(getPlayer().getPos().x - 10, BOTTOM));
						}

						break;
					case VK_SPACE:
						shootBullet();
						break;
					default:
						break;
				}
			}
		}
	}

	
	
}
