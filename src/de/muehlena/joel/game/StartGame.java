package de.muehlena.joel.game;

import name.panitz.game.framework.swing.SwingGame;

public class StartGame {

	public static void main(String[] args) {
		SwingGame.startGame(new Spiel<>());
	}

}
